FROM ubuntu:20.04

USER root

ENV NODE_VERSION=v16.9.0 DISTRO=linux-x64
ENV ANDROID_HOME=/android-sdk
ENV PATH=/usr/local/lib/nodejs/node-$NODE_VERSION-$DISTRO/bin:$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools DEBIAN_FRONTEND=noninteractive

# Install all system requirements
RUN apt-get update && apt-get install -y sudo lib32z1 lib32ncurses5-dev g++ unzip openjdk-8-jdk zsh-common curl gnupg2 git make libx32gcc-8-dev xxd docker.io wget && \
    sudo mkdir -p /usr/local/lib/nodejs && \
    wget https://nodejs.org/dist/$NODE_VERSION/node-$NODE_VERSION-$DISTRO.tar.gz && \
    sudo tar -xvf node-$NODE_VERSION-$DISTRO.tar.gz -C /usr/local/lib/nodejs && \
    rm node-$NODE_VERSION-$DISTRO.tar.gz && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/lib/apt/*


RUN groupadd nsgroup && \
    useradd -m -g nsgroup nsuser

# Allow the nsuser to use sudo without a password (helps when debugging configuration issues)
RUN echo "nsuser ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/nsuser && \
    chmod 0440 /etc/sudoers.d/nsuser && \
    echo "Defaults:nsuser !secure_path" >> /etc/sudoers && \
    sed -i -e "s/secure_path=\"/secure_path=\"\/usr\/local\/lib\/nodejs\/node-${NODE_VERSION}-${DISTRO}:/" /etc/sudoers

# Switch to non-root nsuser
USER nsuser

# Install android sdk
RUN curl "https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip" -o /tmp/sdk.zip && \
    sudo mkdir -p /android-sdk && \
    sudo chmod a+w /android-sdk && \
    unzip -q /tmp/sdk.zip -d /android-sdk && \
    mkdir -p ~/.android/ && touch ~/.android/repositories.cfg && \
    rm /tmp/sdk.zip

RUN echo "export JAVA_OPTS=\"$JAVA_OPTS\"" >> ~/.bashrc && \
    echo "export ANDROID_HOME=$ANDROID_HOME" >> ~/.bashrc && \
    echo "export PATH=$PATH" >> ~/.bashrc

# Download sdk components (takes a long time)
RUN yes | /android-sdk/tools/bin/sdkmanager --licenses && \
    /android-sdk/tools/bin/sdkmanager "tools" "platform-tools" "platforms;android-30" "build-tools;30.0.3" "extras;google;m2repository" "extras;android;m2repository"

# Install nativescript globally
RUN yes | sudo npm install nativescript@8.0.2 -g

# Configure reporting, and ensure that we have a good setup (will fail if 'ns doctor' finds any issues)
RUN node -v && \
    npm -v && \
    sudo node -v && \
    sudo npm -v && \
    ns usage-reporting disable && \
    ns error-reporting enable && \
    ns doctor
